import * as RouterModule from './routes/route-manager.js';
document.addEventListener('DOMContentLoaded', ready);

function ready() {
  /* Button for Filters for < 980 screen */
  (function () {
    var filterBtnAside = document.querySelector('.aside__filter-button');
    var filtersContainer = document.querySelector('.aside__aside-container');
    filterBtnAside.addEventListener('click', function () {
      filtersContainer.classList.toggle('js-aside');
    });
  })();
  /* DropDown List */


  (function () {
    /* ABOUT US DROPDOWN*/
    var itemAboutUs = document.querySelector('.navbar__item-aboutus'); //li

    var itemAboutUsIcon = document.querySelector('.fa-angle-down'); // icon ^

    var aboutUsDropdown = document.querySelector('.navbar__item-aboutus-dropdown'); // drop-down-list

    itemAboutUs.addEventListener('click', function (event) {
      toggleDropDown();
      event.stopPropagation();
    });
    window.addEventListener('click', function (event) {
      if (event.target !== itemAboutUs && aboutUsDropdown.classList.contains('show-dropdown')) {
        toggleDropDown();
      }
    });

    function toggleDropDown() {
      aboutUsDropdown.classList.toggle('show-dropdown');
      itemAboutUsIcon.classList.toggle('rotate');
    }
  })();
  /* HAMBURGER Button */


  (function () {
    var hamburgerButton = document.querySelector('.hamburger-button');
    var hamburgerBtnIcon = document.querySelector('.hamburger-button__icon');
    var hamburgerNavBar = document.querySelector('.header__header-navbar');
    var hamburgerRegistration = document.querySelector('.header__registration');
    hamburgerButton.addEventListener('click', function () {
      if (hamburgerBtnIcon.classList.contains('fa-bars')) {
        hamburgerBtnIcon.classList.replace('fa-bars', 'fa-times');
      } else {
        hamburgerBtnIcon.classList.replace('fa-times', 'fa-bars');
      }

      hamburgerNavBar.classList.toggle('js-hamburger');
      hamburgerRegistration.classList.toggle('js-hamburger');
    });
    window.addEventListener('hashchange', function () {
      hamburgerNavBar.classList.toggle('js-hamburger');
      hamburgerRegistration.classList.toggle('js-hamburger');
      hamburgerBtnIcon.classList.replace('fa-times', 'fa-bars');
    });
  })();
  /* SLIDER */


  (function () {
    var headerSliderBackgroundsList = ["./assets/img/header-slider/header-slide1.png", "./assets/img/header-slider/header-slide2.png", "./assets/img/header-slider/header-slide3.png", "./assets/img/header-slider/header-slide4.png"];
    var sliderContainer = document.querySelector('.slider__slider-container');
    var arrowPrev = document.querySelector('.slider--slider-prev-arrow');
    var arrowNext = document.querySelector('.slider--slider-next-arrow');
    var slide = 0;
    sliderContainer.style.backgroundImage = "url(".concat(headerSliderBackgroundsList[slide], ")");

    function indexChecker(slide) {
      if (slide === headerSliderBackgroundsList.length) {
        slide = 0;
      }

      if (slide < 0) {
        slide = headerSliderBackgroundsList.length - 1;
      }

      return slide;
    }

    arrowNext.addEventListener('click', function () {
      slide++;
      slide = indexChecker(slide);
      sliderContainer.style.backgroundImage = "url(".concat(headerSliderBackgroundsList[slide], ")");
    });
    arrowPrev.addEventListener('click', function () {
      slide--;
      slide = indexChecker(slide);
      sliderContainer.style.backgroundImage = "url(".concat(headerSliderBackgroundsList[slide], ")");
    });
  })();
  /* Button Scroll to TOP */


  (function () {
    var btnTop = document.querySelector('.btn-top');
    btnTop.style.display = 'none';
    window.addEventListener('scroll', function () {
      if (document.body.scrollTop > window.innerHeight || document.documentElement.scrollTop > window.innerHeight) {
        btnTop.style.display = 'block';
      } else {
        btnTop.style.display = 'none';
      }
    });
    btnTop.addEventListener('click', function () {
      scrollToTop(900);
    });

    function scrollToTop(scrollDuration) {
      var scrollStep = -window.scrollY / (scrollDuration / 15),
          scrollInterval = setInterval(function () {
        if (window.scrollY !== 0) {
          window.scrollBy(0, scrollStep);
        } else clearInterval(scrollInterval);
      }, 15);
    }
  })();
}