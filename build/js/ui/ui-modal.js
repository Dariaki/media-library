function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * UI class is responsible for making modal window and its parts layouts: fields, form, close and submit buttons.
 *
 * @class UiModal
 *
 * */
var UiModal =
/*#__PURE__*/
function () {
  function UiModal() {
    _classCallCheck(this, UiModal);
  }

  _createClass(UiModal, null, [{
    key: "createModalContainer",

    /**
     * Creates Container for modal window and its content
     *
     * @param closeModalBtn - button for closing modal window
     * @param form - form element
     *
     * @return {HTMLElement} - modal container
     *
     */
    value: function createModalContainer(closeModalBtn, form) {
      var container = document.createElement('div');
      container.classList.add('item-modal');
      var content = document.createElement('div');
      content.classList.add('item-modal-content');
      content.append(closeModalBtn, form);
      container.append(content);
      return container;
    }
    /**
     * Creates Button to close modal
     *
     * @return {HTMLButtonElement} - <button>
     *
     */

  }, {
    key: "createCloseButton",
    value: function createCloseButton() {
      var closeButton = document.createElement('span');
      closeButton.classList.add('close-button');
      closeButton.innerHTML = "&times;";
      return closeButton;
    }
    /**
     * Creates Form Element
     *
     * @param {HTMLElement} title - heading element
     * @param {array} inputs - list of <input>'s
     * @param {HTMLButtonElement} buttonSubmit
     * @param identifier - class selector for specific modal, e.g. --add-form, --signup-form
     *
     * @return {HTMLElement} - wrapper container that contains <form> element
     *
     */

  }, {
    key: "createForm",
    value: function createForm(title, inputs, buttonSubmit, identifier) {
      var formContainer = document.createElement('div');
      formContainer.classList.add('content-list');
      var form = document.createElement('form');
      form.action = '/';
      form.method = 'GET';
      form.classList.add('modal-form', "".concat(identifier));
      form.append(title);
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = inputs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var input = _step.value;
          form.append(input);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      form.append(buttonSubmit);
      formContainer.append(form);
      return formContainer;
    }
    /**
     * Creates Input Field Element
     *
     * @param type - input attribute type
     * @param name - input attribute name as well as innerHTML content
     * @param id - input attribute id
     * @param placeholder - input attribute placeholder
     *
     * @return {HTMLElement} - wrapper container that contains <input> element
     *
     */

  }, {
    key: "createField",
    value: function createField(type, name, id, placeholder) {
      var container = document.createElement('div');
      container.classList.add('modal-form__field-container');
      container.innerHTML = "\n                <div class=\"modal-form__label-container\">\n                    <label \n                        for=\"".concat(id, "\">\n                                ").concat(name, "\n                    </label>\n                </div>\n                <input \n                    required\n                    type=\"").concat(type, "\" \n                    id=\"").concat(id, "\" \n                    name=\"").concat(name, "\" \n                    placeholder=\"").concat(placeholder, "\" \n                    class=\"field modal-form__field\">\n            ");
      return container;
    }
    /**
     * Creates Option for Select tag
     *
     * @param value - option tag value attribute
     * @param name - option tag name
     *
     * @return {HTMLElement} - <option> tag
     *
     */

  }, {
    key: "createSelectOption",
    value: function createSelectOption(value, name) {
      var option = document.createElement('option');
      option.setAttribute('value', value);
      option.innerText = "".concat(name);
      return option;
    }
    /**
     * Creates Select Field Element
     *
     * @param name - select tag attribute name
     * @param id - select tag id
     * @param options - array of <option> tags
     *
     * @return {HTMLElement} - wrapper container that contains <select> element and its label
     *
     */

  }, {
    key: "createSelectField",
    value: function createSelectField(name, id, options) {
      var container = document.createElement('div');
      container.classList.add('modal-form__field-container');
      var selectLabelContainer = document.createElement('div');
      selectLabelContainer.classList.add('modal-form__label-container');
      var selectLabel = document.createElement('label');
      selectLabel.setAttribute('for', id);
      selectLabel.innerText = "".concat(name);
      selectLabelContainer.appendChild(selectLabel);
      var selectTag = document.createElement('select');
      selectTag.setAttribute('name', name);
      selectTag.setAttribute('id', id);
      selectTag.classList.add('field', 'modal-form__field');
      container.appendChild(selectLabelContainer);
      container.appendChild(selectTag);
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = options[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var option = _step2.value;
          selectTag.appendChild(option);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      return container;
    }
    /**
     * Creates Title for <form> element
     *
     * @param formTitle - innerText for heading element
     * @return {HTMLElement} - heading element
     *
     */

  }, {
    key: "createFormTitle",
    value: function createFormTitle(formTitle) {
      var title = document.createElement('h3');
      title.classList.add('modal-form__title');
      title.innerText = "".concat(formTitle);
      return title;
    }
    /**
     * Creates Submit Button for <form> element
     *
     * @param name - innerText for <button> element
     * @return {HTMLButtonElement}
     *
     */

  }, {
    key: "createSubmitButton",
    value: function createSubmitButton(name) {
      var submitButton = document.createElement('button');
      submitButton.classList.add('btn', '--btn-modal');
      submitButton.innerText = "".concat(name);
      submitButton.setAttribute('type', 'submit');
      return submitButton;
    }
  }]);

  return UiModal;
}();