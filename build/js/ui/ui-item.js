function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * This UI class is responsible for making html layout of item object;
 *
 * @class UIItem
 *
 * */
var UIItem =
/*#__PURE__*/
function () {
  function UIItem(gallery) {
    _classCallCheck(this, UIItem);

    /**
       * @param {HTMLElement} gallery - in which to add objects in;
     *
     * */
    this.gallery = gallery;
  }
  /**
   *  Function addItem() is invoked for each object item from items-list.js
   * */


  _createClass(UIItem, [{
    key: "displayItems",
    value: function displayItems(array) {
      var _this = this;

      array.forEach(function (item) {
        return _this.addItem(_objectSpread({}, item));
      });
    }
    /**
     *  Clears gallery from items;
     *
     * */

  }, {
    key: "removeItems",
    value: function removeItems() {
      this.gallery.innerHTML = '';
    }
    /**
     * Method creates <li> from item object and puts it into <ul>
     * Depending on item.category (video or not) we form corresponding layout
     * Depending on item.language - proper image url name is chosen.
     *
     * @param {object} item
     *
     * */

  }, {
    key: "addItem",
    value: function addItem(item) {
      var element = document.createElement('li');
      element.classList.add('main-gallery-item');
      element.classList.add('main-gallery__item');
      element.setAttribute('id', "".concat(item.id));
      var category = item.category.toLowerCase();
      var mediaContent;

      if (category === 'video' && item.link.includes('youtube')) {
        var linkTransformed = UIItem.linkTransform(item, item.link);
        mediaContent = "\n                    <iframe\n                        class=\"main-gallery-item__img\"\n                        src=\"".concat(linkTransformed, "\"\n                        frameborder=\"0\"\n                        allowfullscreen>\n                    </iframe>");
      } else {
        mediaContent = "<img \n                    class=\"main-gallery-item__img\" \n                    src=\"./assets/img/main/".concat(item.language.toLowerCase(), ".png\" \n                    alt=\"Language Logo\">\n                    <div class=\"main-gallery-item__image-cover\">Click to view</div>");
      }

      element.innerHTML = "\n            <a \n                href=\"".concat(item.link, "\" \n                class=\"main-gallery-item__item-container\" \n                target=\"_blank\">\n                <div class=\"main-gallery-item__img-container\">\n                    ").concat(mediaContent, "\n                </div>\n            </a>\n            <div class=\"main-gallery-item__content\">\n                <div class=\"main-gallery-item__content-header\">\n                    <h4 class=\"main-gallery-item__title\">\n                        <a \n                            class=\"main-gallery-item__title-link\" \n                            href=\"").concat(item.link, "\" \n                            title=\"").concat(item.title, "\" \n                            target=\"_blank\">\n                                ").concat(item.title, "\n                        </a>\n                    </h4>\n                    <i class=\"far fa-heart favourite\"></i>\n                </div>\n\n                <p class=\"main-gallery-item__language\">\n                    <i class=\"fas fa-laptop-code main-gallery-item__type-icon\"></i>\n                    ").concat(item.language, "\n                </p>\n                <p class=\"main-gallery-item__level\">\n                    <i class=\"fas fa-user-graduate main-gallery-item__type-icon\"></i>\n                    Level: ").concat(item.level, "\n                </p>\n                <p class=\"main-gallery-item__category\">\n                    <i class=\"fas fa-book-open main-gallery-item__type-icon\"></i>\n                    Category: ").concat(item.category, "\n                </p>\n            </div>\n        ");
      this.gallery.append(element);
    }
    /**
     * In order to insert video from YouTube we should substitute "watch?v=" to "embed/" in the url
     *  as well as cut off everything that goes after & in case this is an episode of the series.
     *
     * @param {object} item
     * @param {string} link - video url
     *
     * @return {string} link - transformed link
     *
     * @example
     *
     *   linkTransform(item, 'https://www.youtube.com/watch?v=FZAO72uTj0M&list=PL0lO_mIqDDFXx5_8RmAqAmD_Cdb9DV-H5&index=1')
     *   // => https://www.youtube.com/embed/FZAO72uTj0M
     *
     * */

  }], [{
    key: "linkTransform",
    value: function linkTransform(item, link) {
      var category = item.category.toLowerCase();

      if (category === 'video') {
        return link.replace(/\w+\?\S{2}/, 'embed/').replace(/&.*/, '');
      } else {
        return link;
      }
    }
  }]);

  return UIItem;
}();