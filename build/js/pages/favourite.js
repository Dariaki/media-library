var returnedArray = LocalStorageManager.getFromLocalStorage('Back-End'); // array

var returnedIndices = LocalStorageManager.getFromLocalStorage('Liked-item-ids');
var favouriteGallery = new UIItem(document.querySelector('.favourite-page__list'));

if (returnedIndices.length !== 0) {
  var favouriteItems = returnedArray.filter(function (obj) {
    return returnedIndices.includes(obj.id);
  });
  favouriteGallery.displayItems(favouriteItems);
}