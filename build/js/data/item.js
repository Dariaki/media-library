function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 *
 * @class Item
 * @classdesc Class represents an object (item of a gallery).
 *
 * */
var Item = function Item(title, link, language, level, category) {
  _classCallCheck(this, Item);

  /**
   * @param {string} - unique item id based on number of milliseconds passed from January 1, 1970
   *
   * */
  this.id = String(+new Date());
  /**
   * @param {string} title - Title of the item
   * @param {string} link - URL to outer source
   * @param {string} language - Programming language
   * @param {string} level - Programming level
   * @param {string} category - Category of the source provided
   *
   * */

  this.title = title;
  this.link = link;
  this.language = language;
  this.level = level;
  this.category = category;
};