/**
 * @type {Object[]} itemsList - Preexisting list of items.
 *
 * */
var itemsList = [// ~~~~~~~~~~~~~~ JavaScript ~~~~~~~~~~~~~~~
{
  id: '1',
  language: 'JavaScript',
  level: 'Beginner',
  title: 'How functions work in Javascript',
  link: 'https://www.youtube.com/watch?v=AY6X5jZZ_JE',
  category: 'video'
}, {
  id: '2',
  language: 'JavaScript',
  level: 'Beginner',
  title: 'Logical operators',
  link: 'https://javascript.info/logical-operators',
  category: 'article'
}, {
  id: '3',
  language: 'JavaScript',
  level: 'Beginner',
  title: 'Data Types in details',
  link: 'https://www.youtube.com/watch?v=808eYu9B9Yw',
  category: 'video'
}, {
  id: '4',
  language: 'JavaScript',
  level: 'Intermediate',
  title: 'Object methods, "this"',
  link: 'https://javascript.info/object-methods',
  category: 'article'
}, {
  id: '5',
  language: 'JavaScript',
  level: 'Intermediate',
  title: 'Programming Foundations with JavaScript, HTML and CSS',
  link: 'https://ru.coursera.org/learn/duke-programming-web',
  category: 'course'
}, {
  id: '6',
  language: 'JavaScript',
  level: 'Intermediate',
  title: 'Map and Set',
  link: 'https://www.youtube.com/watch?v=hLgUTM3FOII',
  category: 'video'
}, {
  id: '7',
  language: 'JavaScript',
  level: 'Advanced',
  title: 'Front-End Web Development with React',
  link: 'https://ru.coursera.org/learn/front-end-react',
  category: 'course'
}, {
  id: '8',
  language: 'JavaScript',
  level: 'Advanced',
  title: 'Learning Closure and Lexical Context',
  link: 'https://www.youtube.com/watch?v=-jysK0nlz7A',
  category: 'video'
}, {
  id: '9',
  language: 'JavaScript',
  level: 'Advanced',
  title: 'Modules, introduction',
  link: 'https://javascript.info/modules-intro',
  category: 'article'
}, // ~~~~~~~~~~~~~~ C++ ~~~~~~~~~~~~~~~
{
  id: '10',
  language: 'C++',
  level: 'Beginner',
  title: 'C++ For Loop',
  link: 'https://www.w3schools.com/cpp/cpp_for_loop.asp',
  category: 'article'
}, {
  id: '11',
  language: 'C++',
  level: 'Beginner',
  link: 'https://www.youtube.com/watch?v=1kLw8kZuccQ',
  title: 'Buckys C++ Programming Tutorials - 32 - Arrays',
  category: 'video'
}, {
  id: '12',
  language: 'C++',
  level: 'Beginner',
  title: 'Object-Oriented Data Structures in C++',
  link: 'https://ru.coursera.org/learn/cs-fundamentals-1',
  category: 'course'
}, {
  id: '13',
  language: 'C++',
  level: 'Intermediate',
  title: 'C++ Tutorial 13 : Advanced Functions',
  link: 'https://www.youtube.com/watch?v=TGOXttOoI0U',
  category: 'video'
}, {
  id: '14',
  language: 'C++',
  level: 'Intermediate',
  title: 'Object-Oriented Data Structures in C++',
  link: 'https://ru.coursera.org/learn/cs-fundamentals-1',
  category: 'course'
}, {
  id: '15',
  language: 'C++',
  level: 'Intermediate',
  title: 'How to use Polymorphism in C++',
  link: 'https://www.w3schools.com/cpp/cpp_polymorphism.asp',
  category: 'article'
}, {
  id: '16',
  language: 'C++',
  level: 'Advanced',
  title: 'C++ For C Programmers, Part A',
  link: 'https://ru.coursera.org/learn/c-plus-plus-a',
  category: 'course'
}, {
  id: '17',
  language: 'C++',
  level: 'Advanced',
  title: 'Module testing on C++ и о CxxTest',
  link: 'https://habr.com/ru/post/69160/',
  category: 'article'
}, // ~~~~~~~~~~~~~~ Python ~~~~~~~~~~~~~~~
{
  id: '18',
  language: 'Python',
  level: 'Beginner',
  title: 'Python Tutorial for Beginners 7: Loops and Iterations - For/While Loops',
  link: 'https://www.youtube.com/watch?v=6iF8Xb7Z3wQ',
  category: 'video'
}, {
  id: '19',
  language: 'Python',
  level: 'Beginner',
  title: 'Python Datetime',
  link: 'https://www.w3schools.com/python/python_datetime.asp',
  category: 'article'
}, {
  id: '20',
  language: 'Python',
  level: 'Beginner',
  title: 'Python for Everybody',
  link: 'https://ru.coursera.org/specializations/python',
  category: 'course'
}, {
  id: '21',
  language: 'Python',
  level: 'Intermediate',
  title: 'Learn how to work with RegEx in Python',
  link: 'https://www.w3schools.com/python/python_regex.asp',
  category: 'article'
}, {
  id: '22',
  language: 'Python',
  level: 'Intermediate',
  title: 'Machine Learning fundamentals',
  link: 'https://www.w3schools.com/python/python_ml_getting_started.asp',
  category: 'article'
}, {
  id: '23',
  language: 'Python',
  level: 'Intermediate',
  title: 'Python and Machine Learning for Asset Management',
  link: 'https://ru.coursera.org/learn/python-machine-learning-for-investment-management',
  category: 'course'
}, {
  id: '24',
  language: 'Python',
  level: 'Advanced',
  title: 'Accounting Data Analytics with Python',
  link: 'https://ru.coursera.org/learn/accounting-data-analytics-python',
  category: 'course'
}, {
  id: '25',
  language: 'Python',
  level: 'Advanced',
  title: 'Learning Django',
  link: 'https://www.youtube.com/watch?v=FZAO72uTj0M&list=PL0lO_mIqDDFXx5_8RmAqAmD_Cdb9DV-H5&index=1',
  category: 'video'
}, {
  id: '26',
  language: 'Python',
  level: 'Advanced',
  title: 'Make your first steps with Django framework',
  link: 'https://docs.djangoproject.com/en/3.0/',
  category: 'article'
}, // ~~~~~~~~~~~~~~ Java ~~~~~~~~~~~~~~~
{
  id: '27',
  language: 'Java',
  level: 'Beginner',
  title: 'Java Programming and Software Engineering Fundamentals',
  link: 'https://ru.coursera.org/specializations/java-programming',
  category: 'course'
}, {
  id: '28',
  language: 'Java',
  level: 'Beginner',
  title: 'How to use methods Java',
  link: 'https://www.w3schools.com/JAVA/java_methods.asp',
  category: 'article'
}, {
  id: '29',
  language: 'Java',
  level: 'Beginner',
  title: 'Learn to math with Java',
  link: 'https://www.w3schools.com/JAVA/java_math.asp',
  category: 'article'
}, {
  id: '30',
  language: 'Java',
  level: 'Beginner',
  title: 'Functional Programming with Java',
  link: 'https://www.youtube.com/watch?v=Mmd_RL6feX8&list=PLgjd7_JBgEE7oL1SHWVGzwKHO-XMhwArl',
  category: 'video'
}, {
  id: '31',
  language: 'Java',
  level: 'Intermediate',
  title: 'Object Oriented Programming in Java',
  link: 'https://ru.coursera.org/specializations/object-oriented-programming',
  category: 'course'
}, {
  id: '32',
  language: 'Java',
  level: 'Intermediate',
  title: 'Java Packages & API',
  link: 'https://www.w3schools.com/JAVA/java_packages.asp',
  category: 'article'
}, {
  id: '33',
  language: 'Java',
  level: 'Intermediate',
  title: 'Enum in Java part 1 | Basics',
  link: 'https://www.youtube.com/watch?v=sI4utYmh7O4',
  category: 'video'
}, {
  id: '34',
  language: 'Java',
  level: 'Intermediate',
  title: 'What is HashMap | Java',
  link: 'https://www.w3schools.com/JAVA/java_hashmap.asp',
  category: 'article'
}, {
  id: '35',
  language: 'Java',
  level: 'Advanced',
  title: 'JSP Introduction | Life Cycle of JSP | JSP Execution Process | Web Engineering Lectures',
  link: 'https://www.youtube.com/watch?v=7wJ4BqPxqZY',
  category: 'video'
}, {
  id: '36',
  language: 'Java',
  level: 'Advanced',
  title: ' Introduction to Servlets',
  link: 'https://www.edureka.co/blog/advanced-java-tutorial#Servlets',
  category: 'article'
}, {
  id: '37',
  language: 'Java',
  level: 'Advanced',
  title: 'Introduction to JDBC',
  link: 'https://www.edureka.co/blog/advanced-java-tutorial#JDBC',
  category: 'article'
}, {
  id: '38',
  language: 'Java',
  level: 'Advanced',
  title: 'Parallel, Concurrent, and Distributed Programming in Java',
  link: 'https://ru.coursera.org/specializations/pcdp',
  category: 'course'
}];