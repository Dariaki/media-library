function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 *
 * @class ModalComposition
 * @classdesc Class is responsible for modal windows composition depending on their type.
 *
 * */
var ModalComposition =
/*#__PURE__*/
function () {
  function ModalComposition() {
    _classCallCheck(this, ModalComposition);
  }

  _createClass(ModalComposition, null, [{
    key: "addModalComposition",

    /**
     *@return {HTMLFormElement} - Form for Adding Items
     * */
    value: function addModalComposition() {
      var languageOptions = [UiModal.createSelectOption('JavaScript', 'JavaScript'), UiModal.createSelectOption('C++', 'C++'), UiModal.createSelectOption('Python', 'Python'), UiModal.createSelectOption('Java', 'Java')];
      var levelOptions = [UiModal.createSelectOption('Beginner', 'Beginner'), UiModal.createSelectOption('Intermediate', 'Intermediate'), UiModal.createSelectOption('Advanced', 'Advanced')];
      var categoryOptions = [UiModal.createSelectOption('video', 'Video'), UiModal.createSelectOption('article', 'Article'), UiModal.createSelectOption('course', 'Course')];
      var fields = [UiModal.createField('text', 'Title', 'title', 'Title'), UiModal.createField('text', 'Link', 'link', 'Link'), UiModal.createSelectField('Language', 'language', languageOptions), UiModal.createSelectField('Level', 'level', levelOptions), UiModal.createSelectField('Category', 'category', categoryOptions)];
      var closeAddBtn = UiModal.createCloseButton();
      closeAddBtn.classList.add('--add-btn-close');
      var addForm = UiModal.createForm(UiModal.createFormTitle('Add Item'), fields, UiModal.createSubmitButton('Submit'), '--add-form');
      return UiModal.createModalContainer(closeAddBtn, addForm);
    }
    /**
     *@return {HTMLFormElement} - Form for Signing Up
     * */

  }, {
    key: "signUpModalComposition",
    value: function signUpModalComposition() {
      var fields = [UiModal.createField('text', 'Name', 'nameSignUp', 'Your Name...'), UiModal.createField('text', 'Last Name', 'lastName', 'Your Last Name...'), UiModal.createField('text', 'Email', 'emailSignUp', 'Email'), UiModal.createField('text', 'Phone', 'phone', 'Phone')];
      var closeSignUpBtn = UiModal.createCloseButton();
      closeSignUpBtn.classList.add('--signup-btn-close');
      var signUpForm = UiModal.createForm(UiModal.createFormTitle('Sign Up'), fields, UiModal.createSubmitButton('Sign Up'), '--signUp-form');
      return UiModal.createModalContainer(closeSignUpBtn, signUpForm);
    }
    /**
     *@return {HTMLFormElement} - Form for Signing In
     * */

  }, {
    key: "signInModalComposition",
    value: function signInModalComposition() {
      var fields = [UiModal.createField('text', 'Name', 'nameSignIn', 'Your Name...'), UiModal.createField('text', 'Email', 'emailSignIn', 'Email')];
      var closeSignInBtn = UiModal.createCloseButton();
      closeSignInBtn.classList.add('--signin-btn-close');
      var signInForm = UiModal.createForm(UiModal.createFormTitle('Sign In'), fields, UiModal.createSubmitButton('Sign In'), '--signIn-form');
      return UiModal.createModalContainer(closeSignInBtn, signInForm);
    }
  }]);

  return ModalComposition;
}();