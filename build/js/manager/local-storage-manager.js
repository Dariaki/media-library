function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 *
 * @class LocalStorageManager
 * @classdesc Class provides methods for sending and receiving items to Local Storage.
 *
 * */
var LocalStorageManager =
/*#__PURE__*/
function () {
  function LocalStorageManager() {
    _classCallCheck(this, LocalStorageManager);
  }

  _createClass(LocalStorageManager, null, [{
    key: "createLocalStorage",

    /**
     * Converts value to json format.
     * Sets Item in LS
     *
     * @param {string} key - key to store item in Local Storage
     * @param {Object[] || []} array - Value which is stored.
     */
    value: function createLocalStorage(key, array) {
      var jsonValue = JSON.stringify(array);
      localStorage.setItem("".concat(key), "".concat(jsonValue));
    }
    /**
     * Receives item from LS
     * Converts value back from json format.
     *
     * @param {string} key - key to get item from Local Storage
     */

  }, {
    key: "getFromLocalStorage",
    value: function getFromLocalStorage(key) {
      var receivedValue = localStorage.getItem("".concat(key));
      return JSON.parse(receivedValue);
    }
  }]);

  return LocalStorageManager;
}();