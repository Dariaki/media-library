/**
 *  Block manages gathering values from Add From and forming Item object
 *
 * */
window.addEventListener("load", onFormReady);

function onFormReady() {
  var addForm = document.querySelector('.--add-form');
  addForm.addEventListener('submit', function () {
    var title = document.querySelector('#title').value;
    var link = document.querySelector('#link').value;
    var language = document.querySelector('#language').value;
    var level = document.querySelector('#level').value;
    var category = document.querySelector('#category').value;
    var item = new Item(title, link, language, level, category);
    var dataToUpdate = LocalStorageManager.getFromLocalStorage('Back-End');
    dataToUpdate.push(item);
    LocalStorageManager.createLocalStorage('Back-End', dataToUpdate); // update LS

    function clearValues(selector) {
      selector.value = '';
    }

    document.querySelectorAll('.modal-form__field').forEach(function (input) {
      return clearValues(input);
    });
  });
}