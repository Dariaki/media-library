window.addEventListener('load', onLikeReady);

function onLikeReady() {
  var galleryOfItems = document.querySelector('.main-gallery'); // ul

  var itemsOfGallery = document.querySelectorAll('.main-gallery-item');

  if (!LocalStorageManager.getFromLocalStorage('Liked-item-ids')) {
    LocalStorageManager.createLocalStorage('Liked-item-ids', []);
  }

  if (LocalStorageManager.getFromLocalStorage('Liked-item-ids').length !== 0) {
    var arrayOfIds = LocalStorageManager.getFromLocalStorage('Liked-item-ids'); // 1, 2, 4

    arrayOfIds.forEach(function (id) {
      itemsOfGallery.forEach(function (item) {
        if (item.getAttribute('id') === id) {
          var heartIcon = item.querySelector('.favourite');
          heartIcon.classList.replace('far', 'fas');
        }
      });
    });
  }

  galleryOfItems.addEventListener('click', function (event) {
    if (event.target.classList.contains('far') && event.target.classList.contains('favourite')) {
      var element = event.target.closest('.main-gallery-item');
      var id = element.getAttribute('id'); // '2441224'

      event.target.classList.replace('far', 'fas');
      LikeManager.addLike(id, 'Liked-item-ids');
    } else if (event.target.classList.contains('fas') && event.target.classList.contains('favourite')) {
      var _element = event.target.closest('.main-gallery-item');

      var _id = _element.getAttribute('id');

      event.target.classList.replace('fas', 'far');
      LikeManager.removeLike(_id, 'Liked-item-ids');
    }
  });
}