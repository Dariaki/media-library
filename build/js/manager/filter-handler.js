window.addEventListener('load', onFilterReady);

function onFilterReady() {
  var dataToFilter = LocalStorageManager.getFromLocalStorage('Back-End');
  var arrayToFilter = new FilterManager(dataToFilter);
  var categoryTabs = document.querySelectorAll('.category-list__item'); // [li, li, li]

  var categoryList = document.querySelector('.category-list'); // ul

  var languageTabs = document.querySelectorAll('.language-list__item');
  var languageList = document.querySelector('.language-list');
  var levelTabs = document.querySelectorAll('.level-list__item');
  var levelList = document.querySelector('.level-list'); // Categories List

  categoryList.addEventListener('click', function (event) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = categoryTabs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var categoryTab = _step.value;

        if (categoryTab === event.target) {
          categoryTab.classList.add('active-category-tab');
          var category = categoryTab.getAttribute('data-target'); // video, article, all, course

          var activeLanguage = document.querySelector('.active-language-tab');
          var language = activeLanguage.getAttribute('data-target');
          var activeLevel = document.querySelector('.active-level-tab');
          var level = activeLevel.getAttribute('data-target');
          mainGallery.removeItems();
          var filteredArr = arrayToFilter.generalFilter(category, language, level);
          mainGallery.displayItems(filteredArr);
        } else {
          categoryTab.classList.remove('active-category-tab');
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }); // Languages

  languageList.addEventListener('click', function (event) {
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
      for (var _iterator2 = languageTabs[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var languageTab = _step2.value;

        if (languageTab === event.target) {
          languageTab.classList.add('active-language-tab');
          var language = languageTab.getAttribute('data-target'); // js, python, c++

          var activeCategory = document.querySelector('.active-category-tab');
          var category = activeCategory.getAttribute('data-target');
          var activeLevel = document.querySelector('.active-level-tab');
          var level = activeLevel.getAttribute('data-target');
          mainGallery.removeItems();
          var filteredArr = arrayToFilter.generalFilter(category, language, level);
          mainGallery.displayItems(filteredArr);
        } else {
          languageTab.classList.remove('active-language-tab');
        }
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }
  }); // Level

  levelList.addEventListener('click', function (event) {
    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
      for (var _iterator3 = levelTabs[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var levelTab = _step3.value;

        if (levelTab === event.target) {
          levelTab.classList.add('active-level-tab');
          var level = levelTab.getAttribute('data-target'); // beginner, intermediate, advanced

          var activeCategory = document.querySelector('.active-category-tab');
          var category = activeCategory.getAttribute('data-target');
          var activeLanguage = document.querySelector('.active-language-tab');
          var language = activeLanguage.getAttribute('data-target');
          mainGallery.removeItems();
          var filteredArr = arrayToFilter.generalFilter(category, language, level);
          mainGallery.displayItems(filteredArr);
        } else {
          levelTab.classList.remove('active-level-tab');
        }
      }
    } catch (err) {
      _didIteratorError3 = true;
      _iteratorError3 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
          _iterator3.return();
        }
      } finally {
        if (_didIteratorError3) {
          throw _iteratorError3;
        }
      }
    }
  }); // TITLE FILTER

  var searchForm = document.querySelector('.search__form');
  var searchField = document.querySelector('.search__field');
  var dataToFilterByTitle = LocalStorageManager.getFromLocalStorage('Back-End');
  var arrayToFilterByTitle = new FilterManager(dataToFilterByTitle);
  searchForm.addEventListener('submit', function (event) {
    event.preventDefault();
    var term = searchField.value; // "substring of Title"  e.g. FuncTION

    mainGallery.removeItems();
    var filteredArray = arrayToFilterByTitle.searchFilter(term);
    mainGallery.displayItems(filteredArray);
    searchField.value = '';
  });
}