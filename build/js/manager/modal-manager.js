function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 *
 * @class ModalManager
 * @classdesc Class provides methods for opening and closing modal windows.
 *
 * */
var ModalManager =
/*#__PURE__*/
function () {
  function ModalManager() {
    _classCallCheck(this, ModalManager);
  }

  _createClass(ModalManager, [{
    key: "openModal",

    /**
     *
     * @param {HTMLButtonElement} buttonOpen - Button which opens modal.
     * @param {HTMLElement} modalName - Block which represents modal window.
     *
     * */
    value: function openModal(buttonOpen, modalName) {
      var _this = this;

      buttonOpen.addEventListener('click', function () {
        _this.toggleModal(modalName);
      });
    }
  }, {
    key: "toggleModal",
    value: function toggleModal(modalName) {
      modalName.classList.toggle("show-modal");
    }
  }, {
    key: "windowOnClick",
    value: function windowOnClick(modalName) {
      if (event.target === modalName) {
        this.toggleModal(modalName);
      }
    }
  }, {
    key: "closeModalOnWindow",
    value: function closeModalOnWindow(modalName) {
      var _this2 = this;

      window.addEventListener("click", function () {
        _this2.windowOnClick(modalName);

        _this2.windowOnClick(modalName);

        _this2.windowOnClick(modalName);
      });
    }
  }, {
    key: "closeModalOnButton",
    value: function closeModalOnButton(button, modalName) {
      var _this3 = this;

      button.addEventListener("click", function () {
        _this3.toggleModal(modalName);
      });
    }
  }]);

  return ModalManager;
}();

document.addEventListener('DOMContentLoaded', onModalReady);

function onModalReady() {
  /**
   *
   * @description This block is responsible for displaying modals in html layout.
   *
   * */
  var modalAdd = ModalComposition.addModalComposition();
  var modalSignUp = ModalComposition.signUpModalComposition();
  var modalSignIn = ModalComposition.signInModalComposition();
  document.body.append(modalAdd);
  document.body.append(modalSignUp);
  document.body.append(modalSignIn);
  /**
   *
   * @description Instances of ModalManager representing manipulations with three modal windows - Add, Sign Up
   * and Sign In.
   *
   * */

  var modalAdding = new ModalManager();
  var modalSigningUp = new ModalManager();
  var modalSigningIn = new ModalManager();
  modalAdding.openModal(document.querySelector('.category__add-btn-container'), modalAdd);
  modalSigningUp.openModal(document.querySelector('.--signup'), modalSignUp);
  modalSigningIn.openModal(document.querySelector('.--signin'), modalSignIn);
  modalAdding.closeModalOnWindow(modalAdd);
  modalSigningUp.closeModalOnWindow(modalSignUp);
  modalSigningIn.closeModalOnWindow(modalSignIn);
  modalAdding.closeModalOnButton(document.querySelector('.--add-btn-close'), modalAdd);
  modalSigningUp.closeModalOnButton(document.querySelector('.--signup-btn-close'), modalSignUp);
  modalSigningIn.closeModalOnButton(document.querySelector('.--signin-btn-close'), modalSignIn);
}