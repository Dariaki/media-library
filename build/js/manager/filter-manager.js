function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
*
* @class FilterManager
* @classdesc Class provides methods for filtering array of objects.
*
* */
var FilterManager =
/*#__PURE__*/
function () {
  function FilterManager(array) {
    _classCallCheck(this, FilterManager);

    /**
     * @param {Object[]} array - The array to be filtered.
     */
    this.array = array;
  }
  /**
   * Filters by tree parameters. When parameter equals to All - no filters applied.
   *
   * @param {string} category -  All/Video/Article/Course
   * @param {string} language -  All/JavaScript/Python/C++/Java
   * @param {string} level - All/Beginner/Intermediate/Advanced
   * @return {Object[]} - New filtered array.
   */


  _createClass(FilterManager, [{
    key: "generalFilter",
    value: function generalFilter(category, language, level) {
      var itemsListFiltered = _toConsumableArray(this.array);

      if (category !== 'all') {
        itemsListFiltered = itemsListFiltered.filter(function (obj) {
          return obj.category === category;
        });
      }

      if (language !== 'all') {
        itemsListFiltered = itemsListFiltered.filter(function (obj) {
          return obj.language === language;
        });
      }

      if (level !== 'all') {
        itemsListFiltered = itemsListFiltered.filter(function (obj) {
          return obj.level === level;
        });
      }

      return itemsListFiltered;
    }
    /**
     * Filters by item title
     * @param {string} query - Substring of title
     * @return {Object[]} - New filtered array
     */

  }, {
    key: "searchFilter",
    value: function searchFilter(query) {
      return this.array.filter(function (obj) {
        var title = obj.title.toLowerCase();
        return title.includes(query.toLowerCase());
      });
    }
  }]);

  return FilterManager;
}();