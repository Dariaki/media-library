function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 *
 * @class LikeManager
 * @classdesc Class provides methods for adding and removing ids of selected items to Local Storage
 *
 * */
var LikeManager =
/*#__PURE__*/
function () {
  function LikeManager() {
    _classCallCheck(this, LikeManager);
  }

  _createClass(LikeManager, null, [{
    key: "addLike",

    /**
     * Requests array of ids from Local Storage
     * Checks if such id already exists, if no - push id
     * Sends array back to LS by its key.
     *
     * @param {string} id - unique object identifier
     * @param {string} key - key of LS item where our array of ids is stored.
     */
    value: function addLike(id, key) {
      var likedDataToUpdate = LocalStorageManager.getFromLocalStorage(key);
      !likedDataToUpdate.includes(id) ? likedDataToUpdate.push(id) : likedDataToUpdate;
      LocalStorageManager.createLocalStorage(key, likedDataToUpdate);
    }
    /**
     * Opposite to addLike methods - removes id from array.
     *
     * @param {string} id - Title of the item
     * @param {string} key - URL to outer source
     */

  }, {
    key: "removeLike",
    value: function removeLike(id, key) {
      var likedDataToRemove = LocalStorageManager.getFromLocalStorage(key); // array old

      var index = likedDataToRemove.indexOf(id);
      likedDataToRemove.splice(index, 1);
      LocalStorageManager.createLocalStorage(key, likedDataToRemove);
    }
  }]);

  return LikeManager;
}();