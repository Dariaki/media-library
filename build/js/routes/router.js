function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 *
 * @class Router
 * @classdesc Router provides methods to find the requested route, connect js logic to the page as well as
 * change the content of the root element as soon as the route was found.
 *
 * */
var Router =
/*#__PURE__*/
function () {
  /**
   * @param {Object[]} routes - The array of route.
   */
  function Router(routes) {
    _classCallCheck(this, Router);

    this.routes = routes;
    /**
     * @param {HTMLElement} - Root element - <div> container where responseText will be stored.
     */

    this.rootElem = document.getElementById('app');
    window.location.hash = '#home';
  }
  /**
   * @method findHtml() - filter array of routes and find the route which matches current window.location.hash
   *
   * @returns {string} - route parameter htmlName, which points at html file - e.g. 'contacts.html'
   */


  _createClass(Router, [{
    key: "findHtml",
    value: function findHtml() {
      return this.routes.find(function (route) {
        return '#' + route.name === window.location.hash;
      }).htmlName;
    }
    /**
     * @method findJS() - filter array of routes and find the route which matches current window.location.hash
     *
     * @returns {string} - route parameter jsName, which points at js file, e.g. './js/pages/contacts.js' || false
     */

  }, {
    key: "findJS",
    value: function findJS() {
      return this.routes.find(function (route) {
        return '#' + route.name === window.location.hash;
      }).jsName;
    }
    /**
     * @method findRoute() - invokes goToRoute method passing two parameters,
     *
     * @example
     *      goToRoute('contacts.html', './js/pages/contacts.js');
     */

  }, {
    key: "findRoute",
    value: function findRoute() {
      this.goToRoute(this.findHtml(), this.findJS());
    }
    /**
     * @method createJS() - creates <script> tag with src attr to the js file, e.g. ./js/pages/contacts.js'
     * @param {string} jsName - src of <script> tag.
     *
     * @returns html script element.
     */

  }, {
    key: "isJSFile",

    /**
     * @method isJSFile() - checked either route parameter './js/pages/contacts.js' or false.
     * Thus, it either appends script tag to the root Element or ignores.
     *
     * @param {string} jsName - url
     *
     */
    value: function isJSFile(jsName) {
      this.findJS() ? this.rootElem.append(Router.createJS(jsName)) : '';
    }
    /**
     * @method goToRoute() - first checks if htmlName is not our home file, if so - it simply removes the content
     * from our root element, if this is another file - we sends xhr request to our view/ directory for the content.
     * In case of success - we inserts responseText to the root element and connects js script to it.
     * In case of reject - error message.
     *
     * @param {string} htmlName - 'contacts.html'
     * @param {string} jsName - ./js/pages/contacts.js'
     *
     */

  }, {
    key: "goToRoute",
    value: function goToRoute(htmlName, jsName) {
      var _this = this;

      if (htmlName !== 'index.html') {
        var url = 'views/' + htmlName;
        var request = new XMLHttpRequest();
        request.addEventListener('load', function () {
          if (request.readyState === 4 && request.status === 200) {
            _this.rootElem.innerHTML = request.responseText;

            _this.isJSFile(jsName);
          } else {
            console.log('Oooops! Nothing was found! Please try again');
          }
        });
        request.open('GET', url, true);
        request.send();
      } else {
        this.rootElem.innerHTML = '';
      }
    }
  }], [{
    key: "createJS",
    value: function createJS(jsName) {
      var script = document.createElement('script');
      script.setAttribute('src', "".concat(jsName));
      return script;
    }
  }]);

  return Router;
}();

export { Router as default };