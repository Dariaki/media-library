function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Route = function Route(name, htmlName) {
  var jsName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  _classCallCheck(this, Route);

  this.name = name;
  this.htmlName = htmlName;
  this.jsName = jsName;
};

export { Route as default };