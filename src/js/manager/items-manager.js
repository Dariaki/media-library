/**
 * Getting preexisting data from our Local Storage and displaying items from array as soon as the document loads.
 * */

// array - pass as parameter to create and use once

document.addEventListener('DOMContentLoaded', onDOMReady);
const mainGallery = new UIItem(document.querySelector('.main-gallery'));

function onDOMReady() {

    if (!LocalStorageManager.getFromLocalStorage('Back-End')) {
        LocalStorageManager.createLocalStorage('Back-End', itemsList);
    }

    let dataToDisplay = LocalStorageManager.getFromLocalStorage('Back-End');

    mainGallery.displayItems(dataToDisplay);

}
