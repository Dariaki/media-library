/**
*
* @class FilterManager
* @classdesc Class provides methods for filtering array of objects.
*
* */

class FilterManager {

        constructor(array) {
            /**
             * @param {Object[]} array - The array to be filtered.
             */
            this.array = array;
        }

        /**
         * Filters by tree parameters. When parameter equals to All - no filters applied.
         *
         * @param {string} category -  All/Video/Article/Course
         * @param {string} language -  All/JavaScript/Python/C++/Java
         * @param {string} level - All/Beginner/Intermediate/Advanced
         * @return {Object[]} - New filtered array.
         */
        generalFilter(category, language, level) {

            let itemsListFiltered = [...this.array];

            if(category !== 'all') { itemsListFiltered = itemsListFiltered.filter(obj => obj.category === category)}
            if(language !== 'all') { itemsListFiltered = itemsListFiltered.filter(obj => obj.language === language)}
            if(level !== 'all') {    itemsListFiltered = itemsListFiltered.filter(obj => obj.level === level)}

            return itemsListFiltered;

        }

        /**
         * Filters by item title
         * @param {string} query - Substring of title
         * @return {Object[]} - New filtered array
         */
        searchFilter(query) {

            return this.array.filter(obj => {
                let title = obj.title.toLowerCase();

                return title.includes(query.toLowerCase());
            });
        }
    }


