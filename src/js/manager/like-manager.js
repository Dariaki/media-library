/**
 *
 * @class LikeManager
 * @classdesc Class provides methods for adding and removing ids of selected items to Local Storage
 *
 * */

class LikeManager {

    /**
     * Requests array of ids from Local Storage
     * Checks if such id already exists, if no - push id
     * Sends array back to LS by its key.
     *
     * @param {string} id - unique object identifier
     * @param {string} key - key of LS item where our array of ids is stored.
     */
    static addLike(id, key) {
        let likedDataToUpdate = LocalStorageManager.getFromLocalStorage(key);
        !likedDataToUpdate.includes(id) ? likedDataToUpdate.push(id) : likedDataToUpdate;
        LocalStorageManager.createLocalStorage(key, likedDataToUpdate);
    }

    /**
     * Opposite to addLike methods - removes id from array.
     *
     * @param {string} id - Title of the item
     * @param {string} key - URL to outer source
     */
    static removeLike(id, key) {
        let likedDataToRemove = LocalStorageManager.getFromLocalStorage(key); // array old
        let index = likedDataToRemove.indexOf(id);
        likedDataToRemove.splice(index, 1);
        LocalStorageManager.createLocalStorage(key, likedDataToRemove);
    }

}





