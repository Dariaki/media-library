
window.addEventListener('load', onFilterReady);

function onFilterReady() {


        let dataToFilter = LocalStorageManager.getFromLocalStorage('Back-End');
        let arrayToFilter = new FilterManager(dataToFilter);

        const categoryTabs = document.querySelectorAll('.category-list__item'); // [li, li, li]
        const categoryList = document.querySelector('.category-list'); // ul

        const languageTabs = document.querySelectorAll('.language-list__item');
        const languageList = document.querySelector('.language-list');

        const levelTabs = document.querySelectorAll('.level-list__item');
        const levelList = document.querySelector('.level-list');

        // Categories List
        categoryList.addEventListener('click', function (event) {

            for (let categoryTab of categoryTabs) {
                if (categoryTab === event.target) {
                    categoryTab.classList.add('active-category-tab');

                    let category = categoryTab.getAttribute('data-target'); // video, article, all, course

                    let activeLanguage = document.querySelector('.active-language-tab');
                    let language = activeLanguage.getAttribute('data-target');

                    let activeLevel = document.querySelector('.active-level-tab');
                    let level = activeLevel.getAttribute('data-target');

                    mainGallery.removeItems();
                    const filteredArr = arrayToFilter.generalFilter(category, language, level);
                    mainGallery.displayItems(filteredArr);

                } else {
                    categoryTab.classList.remove('active-category-tab');
                }
            }


        });


        // Languages
        languageList.addEventListener('click', function (event) {

            for (let languageTab of languageTabs) {
                if (languageTab === event.target) {
                    languageTab.classList.add('active-language-tab');

                    let language = languageTab.getAttribute('data-target'); // js, python, c++

                    let activeCategory = document.querySelector('.active-category-tab');
                    let category = activeCategory.getAttribute('data-target');

                    let activeLevel = document.querySelector('.active-level-tab');
                    let level = activeLevel.getAttribute('data-target');

                    mainGallery.removeItems();
                    const filteredArr = arrayToFilter.generalFilter(category, language, level);
                    mainGallery.displayItems(filteredArr);


                } else {
                    languageTab.classList.remove('active-language-tab');
                }

            }
        });

        // Level
        levelList.addEventListener('click', function (event) {

            for (let levelTab of levelTabs) {
                if (levelTab === event.target) {
                    levelTab.classList.add('active-level-tab');

                    let level = levelTab.getAttribute('data-target'); // beginner, intermediate, advanced

                    let activeCategory = document.querySelector('.active-category-tab');
                    let category = activeCategory.getAttribute('data-target');

                    let activeLanguage = document.querySelector('.active-language-tab');
                    let language = activeLanguage.getAttribute('data-target');

                    mainGallery.removeItems();
                    const filteredArr = arrayToFilter.generalFilter(category, language, level);
                    mainGallery.displayItems(filteredArr);


                } else {
                    levelTab.classList.remove('active-level-tab');
                }

            }
        });

// TITLE FILTER
        const searchForm = document.querySelector('.search__form');
        const searchField = document.querySelector('.search__field');
        let dataToFilterByTitle = LocalStorageManager.getFromLocalStorage('Back-End');
        let arrayToFilterByTitle = new FilterManager(dataToFilterByTitle);

        searchForm.addEventListener('submit', function (event) {
            event.preventDefault();

            let term = searchField.value; // "substring of Title"  e.g. FuncTION
            mainGallery.removeItems();
            const filteredArray = arrayToFilterByTitle.searchFilter(term);
            mainGallery.displayItems(filteredArray);

            searchField.value = '';

        });

}