
window.addEventListener('load', onLikeReady);


function onLikeReady() {

    const galleryOfItems = document.querySelector('.main-gallery'); // ul
    let itemsOfGallery = document.querySelectorAll('.main-gallery-item');


    if (!LocalStorageManager.getFromLocalStorage('Liked-item-ids')) {
        LocalStorageManager.createLocalStorage('Liked-item-ids', []);
    }

    if (LocalStorageManager.getFromLocalStorage('Liked-item-ids').length !== 0) {

        let arrayOfIds = LocalStorageManager.getFromLocalStorage('Liked-item-ids'); // 1, 2, 4
        arrayOfIds.forEach(id => {
            itemsOfGallery.forEach(item => {
                if(item.getAttribute('id') === id) {
                    let heartIcon = item.querySelector('.favourite');
                    heartIcon.classList.replace('far', 'fas');
                }
            })
        })
    }


    galleryOfItems.addEventListener('click', function(event) {
        if (event.target.classList.contains('far') &&
            event.target.classList.contains('favourite')) {

            const element = event.target.closest('.main-gallery-item');
            const id = element.getAttribute('id'); // '2441224'

            event.target.classList.replace('far', 'fas');

            LikeManager.addLike(id, 'Liked-item-ids');


        } else if (event.target.classList.contains('fas') &&
            event.target.classList.contains('favourite')) {

            const element = event.target.closest('.main-gallery-item');
            const id = element.getAttribute('id');

            event.target.classList.replace('fas', 'far');

            LikeManager.removeLike(id, 'Liked-item-ids');

        }

    });


}