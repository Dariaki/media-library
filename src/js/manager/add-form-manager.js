/**
 *  Block manages gathering values from Add From and forming Item object
 *
 * */

window.addEventListener("load", onFormReady);


function onFormReady() {

    const addForm = document.querySelector('.--add-form');

    addForm.addEventListener('submit', function () {

        const title = document.querySelector('#title').value;
        const link = document.querySelector('#link').value;

        const language = document.querySelector('#language').value;
        const level = document.querySelector('#level').value;
        const category = document.querySelector('#category').value;

        const item = new Item(
            title,
            link,
            language,
            level,
            category
        );


        let dataToUpdate = LocalStorageManager.getFromLocalStorage('Back-End');
        dataToUpdate.push(item);
        LocalStorageManager.createLocalStorage('Back-End', dataToUpdate); // update LS

        function clearValues(selector) {
            selector.value = '';
        }

        document.querySelectorAll('.modal-form__field').forEach(input => clearValues(input));

    });




}