
/**
 *
 * @class ModalManager
 * @classdesc Class provides methods for opening and closing modal windows.
 *
 * */

class ModalManager {

    /**
     *
     * @param {HTMLButtonElement} buttonOpen - Button which opens modal.
     * @param {HTMLElement} modalName - Block which represents modal window.
     *
     * */
    openModal(buttonOpen, modalName) {

        buttonOpen.addEventListener('click',() => {
            this.toggleModal(modalName)
        });
    }

    toggleModal(modalName) {

        modalName.classList.toggle("show-modal");
    }

    windowOnClick(modalName) {
        if (event.target === modalName) {
            this.toggleModal(modalName);
        }
    }

    closeModalOnWindow(modalName) {

        window.addEventListener("click",() => {
            this.windowOnClick(modalName);
            this.windowOnClick(modalName);
            this.windowOnClick(modalName);
        });
    }

    closeModalOnButton(button, modalName) {

        button.addEventListener("click", () => {
            this.toggleModal(modalName);
        });
    }
}

document.addEventListener('DOMContentLoaded', onModalReady);

function onModalReady() {
    /**
     *
     * @description This block is responsible for displaying modals in html layout.
     *
     * */
    const modalAdd = ModalComposition.addModalComposition();
    const modalSignUp = ModalComposition.signUpModalComposition();
    const modalSignIn = ModalComposition.signInModalComposition();

    document.body.append(modalAdd);
    document.body.append(modalSignUp);
    document.body.append(modalSignIn);

    /**
     *
     * @description Instances of ModalManager representing manipulations with three modal windows - Add, Sign Up
     * and Sign In.
     *
     * */
    const modalAdding = new ModalManager();
    const modalSigningUp = new ModalManager();
    const modalSigningIn = new ModalManager();

    modalAdding.openModal(document.querySelector('.category__add-btn-container'), modalAdd);
    modalSigningUp.openModal(document.querySelector('.--signup'), modalSignUp);
    modalSigningIn.openModal(document.querySelector('.--signin'), modalSignIn);

    modalAdding.closeModalOnWindow(modalAdd);
    modalSigningUp.closeModalOnWindow(modalSignUp);
    modalSigningIn.closeModalOnWindow(modalSignIn);

    modalAdding.closeModalOnButton(document.querySelector('.--add-btn-close'), modalAdd);
    modalSigningUp.closeModalOnButton(document.querySelector('.--signup-btn-close'), modalSignUp);
    modalSigningIn.closeModalOnButton(document.querySelector('.--signin-btn-close'), modalSignIn);

}