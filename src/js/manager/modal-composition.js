
/**
 *
 * @class ModalComposition
 * @classdesc Class is responsible for modal windows composition depending on their type.
 *
 * */


class ModalComposition {

    /**
     *@return {HTMLFormElement} - Form for Adding Items
     * */
        static addModalComposition() {
               const languageOptions = [
                   UiModal.createSelectOption('JavaScript', 'JavaScript'),
                   UiModal.createSelectOption('C++', 'C++'),
                   UiModal.createSelectOption('Python', 'Python'),
                   UiModal.createSelectOption('Java', 'Java'),
               ];

               const levelOptions = [
                   UiModal.createSelectOption('Beginner', 'Beginner'),
                   UiModal.createSelectOption('Intermediate', 'Intermediate'),
                   UiModal.createSelectOption('Advanced', 'Advanced')
               ];

               const categoryOptions = [
                   UiModal.createSelectOption('video', 'Video'),
                   UiModal.createSelectOption('article', 'Article'),
                   UiModal.createSelectOption('course', 'Course'),
               ];

                const fields = [
                    UiModal.createField('text', 'Title', 'title','Title'),
                    UiModal.createField('text', 'Link', 'link','Link'),
                    UiModal.createSelectField('Language', 'language', languageOptions),
                    UiModal.createSelectField('Level', 'level', levelOptions),
                    UiModal.createSelectField('Category', 'category', categoryOptions),
                ];

                const closeAddBtn = UiModal.createCloseButton();
                closeAddBtn.classList.add('--add-btn-close');
               const addForm = UiModal.createForm(UiModal.createFormTitle('Add Item'), fields, UiModal.createSubmitButton('Submit'), '--add-form');
               return UiModal.createModalContainer(closeAddBtn, addForm);
        }

        /**
         *@return {HTMLFormElement} - Form for Signing Up
         * */
        static  signUpModalComposition() {
                const fields = [
                    UiModal.createField('text', 'Name', 'nameSignUp','Your Name...'),
                    UiModal.createField('text', 'Last Name', 'lastName','Your Last Name...'),
                    UiModal.createField('text', 'Email', 'emailSignUp','Email'),
                    UiModal.createField('text', 'Phone', 'phone','Phone'),
                ];

                const closeSignUpBtn = UiModal.createCloseButton();
                closeSignUpBtn.classList.add('--signup-btn-close');
                const signUpForm = UiModal.createForm(UiModal.createFormTitle('Sign Up'), fields, UiModal.createSubmitButton('Sign Up'), '--signUp-form');
                return UiModal.createModalContainer(closeSignUpBtn, signUpForm);
            }

        /**
         *@return {HTMLFormElement} - Form for Signing In
         * */
        static signInModalComposition() {
                const fields = [
                    UiModal.createField('text', 'Name', 'nameSignIn','Your Name...'),
                    UiModal.createField('text', 'Email', 'emailSignIn','Email'),
                ];

                const closeSignInBtn = UiModal.createCloseButton();
                closeSignInBtn.classList.add('--signin-btn-close');
                const signInForm = UiModal.createForm(UiModal.createFormTitle('Sign In'), fields, UiModal.createSubmitButton('Sign In'), '--signIn-form');
                return UiModal.createModalContainer(closeSignInBtn, signInForm);
            }

    }

