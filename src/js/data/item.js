/**
 *
 * @class Item
 * @classdesc Class represents an object (item of a gallery).
 *
 * */

class Item {
        constructor(title, link, language, level, category) {
            /**
             * @param {string} - unique item id based on number of milliseconds passed from January 1, 1970
             *
             * */
            this.id = String(+new Date());
            /**
             * @param {string} title - Title of the item
             * @param {string} link - URL to outer source
             * @param {string} language - Programming language
             * @param {string} level - Programming level
             * @param {string} category - Category of the source provided
             *
             * */
            this.title = title;
            this.link = link;
            this.language = language;
            this.level = level;
            this.category = category;

        }
    }