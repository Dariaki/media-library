export default class Route{
    constructor(name, htmlName, jsName = false) {
        this.name = name;
        this.htmlName = htmlName;
        this.jsName = jsName;
    }
}