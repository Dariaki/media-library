import Router from './router.js';
import Route from './route.js';

const router = new Router([
    new Route('home', 'index.html'),
    new Route('gallery', 'gallery.html'),
    new Route('contacts', 'contacts.html', './js/pages/contacts.js'),
    new Route('favourite', 'favourite.html', './js/pages/favourite.js'),
    new Route('about-achievements', 'about-achievements.html'),
    new Route('about-events', 'about-events.html'),
    new Route('about-team', 'about-team.html'),
]);

window.addEventListener('hashchange', () => {
    if (router.findHtml() === 'index.html') {
        router.findRoute();
        document.querySelector('.home-container').style.display = 'block';
    } else {
        document.querySelector('.home-container').style.display = 'none';
        router.findRoute();
    }
});

