/**
 *
 * @class Router
 * @classdesc Router provides methods to find the requested route, connect js logic to the page as well as
 * change the content of the root element as soon as the route was found.
 *
 * */

export default
    class Router {
        /**
         * @param {Object[]} routes - The array of route.
         */
        constructor(routes) {
            this.routes = routes;
            /**
             * @param {HTMLElement} - Root element - <div> container where responseText will be stored.
             */
            this.rootElem = document.getElementById('app');
            window.location.hash = '#home';
        }


        /**
         * @method findHtml() - filter array of routes and find the route which matches current window.location.hash
         *
         * @returns {string} - route parameter htmlName, which points at html file - e.g. 'contacts.html'
         */
        findHtml() {
            return this.routes.find(route => {
                return '#' + route.name === window.location.hash;
            }).htmlName;
        }


        /**
         * @method findJS() - filter array of routes and find the route which matches current window.location.hash
         *
         * @returns {string} - route parameter jsName, which points at js file, e.g. './js/pages/contacts.js' || false
         */
        findJS() {
            return this.routes.find(route => {
                return '#' + route.name === window.location.hash;
            }).jsName;
        }

        /**
         * @method findRoute() - invokes goToRoute method passing two parameters,
         *
         * @example
         *      goToRoute('contacts.html', './js/pages/contacts.js');
         */
        findRoute() {
            this.goToRoute(this.findHtml(), this.findJS());
        }


        /**
         * @method createJS() - creates <script> tag with src attr to the js file, e.g. ./js/pages/contacts.js'
         * @param {string} jsName - src of <script> tag.
         *
         * @returns html script element.
         */
        static createJS(jsName) {
            let script = document.createElement('script');
            script.setAttribute('src', `${jsName}`);

            return script;
        }

        /**
         * @method isJSFile() - checked either route parameter './js/pages/contacts.js' or false.
         * Thus, it either appends script tag to the root Element or ignores.
         *
         * @param {string} jsName - url
         *
         */
        isJSFile(jsName) {
             this.findJS() ? this.rootElem.append(Router.createJS(jsName)) : ''
        }

        /**
         * @method goToRoute() - first checks if htmlName is not our home file, if so - it simply removes the content
         * from our root element, if this is another file - we sends xhr request to our view/ directory for the content.
         * In case of success - we inserts responseText to the root element and connects js script to it.
         * In case of reject - error message.
         *
         * @param {string} htmlName - 'contacts.html'
         * @param {string} jsName - ./js/pages/contacts.js'
         *
         */
        goToRoute(htmlName, jsName) {
            if (htmlName !== 'index.html') {

                let url = 'views/' + htmlName;

                let request = new XMLHttpRequest();

                request.addEventListener('load', () => {

                    if (request.readyState === 4 && request.status === 200) {

                        this.rootElem.innerHTML = request.responseText;
                        this.isJSFile(jsName);

                    } else {
                        console.log('Oooops! Nothing was found! Please try again');
                    }
                });

                request.open('GET', url, true);
                request.send();
            } else {
                this.rootElem.innerHTML = '';
            }

        }


    }
