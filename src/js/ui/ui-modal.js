/**
 * UI class is responsible for making modal window and its parts layouts: fields, form, close and submit buttons.
 *
 * @class UiModal
 *
 * */

class UiModal {

        /**
         * Creates Container for modal window and its content
         *
         * @param closeModalBtn - button for closing modal window
         * @param form - form element
         *
         * @return {HTMLElement} - modal container
         *
         */
        static createModalContainer(closeModalBtn, form) {
            const container = document.createElement('div');
            container.classList.add('item-modal');

            const content = document.createElement('div');
            content.classList.add('item-modal-content');
            content.append(closeModalBtn, form);

            container.append(content);

            return container;
        }

        /**
         * Creates Button to close modal
         *
         * @return {HTMLButtonElement} - <button>
         *
         */
        static createCloseButton() {
            const closeButton = document.createElement('span');
            closeButton.classList.add('close-button');
            closeButton.innerHTML = `&times;`;

            return closeButton;
        }

        /**
         * Creates Form Element
         *
         * @param {HTMLElement} title - heading element
         * @param {array} inputs - list of <input>'s
         * @param {HTMLButtonElement} buttonSubmit
         * @param identifier - class selector for specific modal, e.g. --add-form, --signup-form
         *
         * @return {HTMLElement} - wrapper container that contains <form> element
         *
         */
        static createForm(title, inputs, buttonSubmit, identifier) {
            const formContainer = document.createElement('div');
            formContainer.classList.add('content-list');

            const form = document.createElement('form');
            form.action = '/';
            form.method = 'GET';
            form.classList.add('modal-form', `${identifier}`);
            form.append(title);

            for (let input of inputs) {
                form.append(input);
            }

            form.append(buttonSubmit);
            formContainer.append(form);

            return formContainer;
        }

        /**
         * Creates Input Field Element
         *
         * @param type - input attribute type
         * @param name - input attribute name as well as innerHTML content
         * @param id - input attribute id
         * @param placeholder - input attribute placeholder
         *
         * @return {HTMLElement} - wrapper container that contains <input> element
         *
         */
        static createField(type, name, id, placeholder) {
            const container = document.createElement('div');
            container.classList.add('modal-form__field-container');

            container.innerHTML = `
                <div class="modal-form__label-container">
                    <label 
                        for="${id}">
                                ${name}
                    </label>
                </div>
                <input 
                    required
                    type="${type}" 
                    id="${id}" 
                    name="${name}" 
                    placeholder="${placeholder}" 
                    class="field modal-form__field">
            `;

            return container;
        }


        /**
         * Creates Option for Select tag
         *
         * @param value - option tag value attribute
         * @param name - option tag name
         *
         * @return {HTMLElement} - <option> tag
         *
         */
        static createSelectOption(value, name) {
            const option = document.createElement('option');
            option.setAttribute('value', value);
            option.innerText = `${name}`;

            return option;
        }

        /**
         * Creates Select Field Element
         *
         * @param name - select tag attribute name
         * @param id - select tag id
         * @param options - array of <option> tags
         *
         * @return {HTMLElement} - wrapper container that contains <select> element and its label
         *
         */
        static createSelectField(name, id, options) {
            const container = document.createElement('div');
            container.classList.add('modal-form__field-container');

            const selectLabelContainer = document.createElement('div');
            selectLabelContainer.classList.add('modal-form__label-container');
            const selectLabel = document.createElement('label');
            selectLabel.setAttribute('for', id);
            selectLabel.innerText = `${name}`;
            selectLabelContainer.appendChild(selectLabel);


            const selectTag = document.createElement('select');
            selectTag.setAttribute('name', name);
            selectTag.setAttribute('id', id);
            selectTag.classList.add('field', 'modal-form__field');

            container.appendChild(selectLabelContainer);
            container.appendChild(selectTag);

            for (let option of options) {
                selectTag.appendChild(option)
            }
            return container;
        }

        /**
         * Creates Title for <form> element
         *
         * @param formTitle - innerText for heading element
         * @return {HTMLElement} - heading element
         *
         */
        static createFormTitle(formTitle) {
            const title = document.createElement('h3');
            title.classList.add('modal-form__title');
            title.innerText = `${formTitle}`;

            return title;
        }

        /**
         * Creates Submit Button for <form> element
         *
         * @param name - innerText for <button> element
         * @return {HTMLButtonElement}
         *
         */
        static createSubmitButton(name) {
            const submitButton = document.createElement('button');
            submitButton.classList.add('btn', '--btn-modal');
            submitButton.innerText = `${name}`;
            submitButton.setAttribute('type', 'submit');
            return submitButton;
        }
    }

