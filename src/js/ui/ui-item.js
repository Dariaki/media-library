/**
 * This UI class is responsible for making html layout of item object;
 *
 * @class UIItem
 *
 * */


class UIItem {

    constructor(gallery) {
        /**

         * @param {HTMLElement} gallery - in which to add objects in;
         *
         * */
        this.gallery = gallery;

    }

    /**
     *  Function addItem() is invoked for each object item from items-list.js
     * */
     displayItems(array) {
        array.forEach((item) => this.addItem({...item}))
    }

    /**
     *  Clears gallery from items;
     *
     * */
    removeItems() {
        this.gallery.innerHTML = '';
    }


    /**
     * Method creates <li> from item object and puts it into <ul>
     * Depending on item.category (video or not) we form corresponding layout
     * Depending on item.language - proper image url name is chosen.
     *
     * @param {object} item
     *
     * */

     addItem(item) {

        const element = document.createElement('li');

        element.classList.add('main-gallery-item');
        element.classList.add('main-gallery__item');

        element.setAttribute('id', `${item.id}`);

        let category = item.category.toLowerCase();

        let mediaContent;

        if (category === 'video' && item.link.includes('youtube')) {

            const linkTransformed = UIItem.linkTransform(item, item.link);
            mediaContent = `
                    <iframe
                        class="main-gallery-item__img"
                        src="${linkTransformed}"
                        frameborder="0"
                        allowfullscreen>
                    </iframe>`;
        } else {
            mediaContent = `<img 
                    class="main-gallery-item__img" 
                    src="./assets/img/main/${item.language.toLowerCase()}.png" 
                    alt="Language Logo">
                    <div class="main-gallery-item__image-cover">Click to view</div>`;
        }

        element.innerHTML = `
            <a 
                href="${item.link}" 
                class="main-gallery-item__item-container" 
                target="_blank">
                <div class="main-gallery-item__img-container">
                    ${mediaContent}
                </div>
            </a>
            <div class="main-gallery-item__content">
                <div class="main-gallery-item__content-header">
                    <h4 class="main-gallery-item__title">
                        <a 
                            class="main-gallery-item__title-link" 
                            href="${item.link}" 
                            title="${item.title}" 
                            target="_blank">
                                ${item.title}
                        </a>
                    </h4>
                    <i class="far fa-heart favourite"></i>
                </div>

                <p class="main-gallery-item__language">
                    <i class="fas fa-laptop-code main-gallery-item__type-icon"></i>
                    ${item.language}
                </p>
                <p class="main-gallery-item__level">
                    <i class="fas fa-user-graduate main-gallery-item__type-icon"></i>
                    Level: ${item.level}
                </p>
                <p class="main-gallery-item__category">
                    <i class="fas fa-book-open main-gallery-item__type-icon"></i>
                    Category: ${item.category}
                </p>
            </div>
        `;

        this.gallery.append(element);
    }

   /**
    * In order to insert video from YouTube we should substitute "watch?v=" to "embed/" in the url
    *  as well as cut off everything that goes after & in case this is an episode of the series.
    *
    * @param {object} item
    * @param {string} link - video url
    *
    * @return {string} link - transformed link
    *
    * @example
    *
    *   linkTransform(item, 'https://www.youtube.com/watch?v=FZAO72uTj0M&list=PL0lO_mIqDDFXx5_8RmAqAmD_Cdb9DV-H5&index=1')
    *   // => https://www.youtube.com/embed/FZAO72uTj0M
    *
    * */
    static linkTransform(item, link) {
        let category = item.category.toLowerCase();
        if (category === 'video') {
            return link
                .replace(/\w+\?\S{2}/, 'embed/')
                .replace(/&.*/, '');
        } else {
            return link;
        }
    }

}