
const returnedArray = LocalStorageManager.getFromLocalStorage('Back-End'); // array
const returnedIndices  = LocalStorageManager.getFromLocalStorage('Liked-item-ids');
const favouriteGallery = new UIItem(document.querySelector('.favourite-page__list'));

if (returnedIndices.length !== 0) {

    const favouriteItems = returnedArray.filter(obj => returnedIndices.includes(obj.id));

    favouriteGallery.displayItems(favouriteItems);

}
