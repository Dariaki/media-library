const mapContainer = document.querySelector('.contacts-page__map-container');
// const map = document.querySelector('.contacts-page__map');
// const imgMap = document.querySelector('.contacts-page__image');


if (window.navigator.onLine) {
    mapContainer.innerHTML = `<iframe
                class="contacts-page__map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2541.521492901351!2d30.468276615697057!3d50.43138637947264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cebe50ea80b7%3A0x42eb8f6bf72636fb!2z0KHQvtC70L7QvNC10L3RgdC60LDRjyDQv9C7Liwg0JrQuNC10LIsIDAyMDAw!5e0!3m2!1sru!2sua!4v1578875095534!5m2!1sru!2sua"
                frameborder="0"
                height="800"
                allowfullscreen="">
        </iframe>`;
} else {
    mapContainer.innerHTML = `<img class="contacts-page__image" src="../assets/img/map-alt.png" alt="Location Map">`;
}



    // const mapUrl = map.getAttribute('src');
    // const mapRequest = new XMLHttpRequest();
    // mapRequest.addEventListener('load', () => {
    //
    //     if (mapRequest.readyState === 4 && mapRequest.status === 200) {
    //         imgMap.style.display = 'none';
    //         map.style.display = 'block';
    //
    //     } else {
    //         map.style.display = 'none';
    //         imgMap.style.display = 'block';
    //     }
    // });
    //
    // mapRequest.open('GET', mapUrl, true);
    // mapRequest.send();
