## Media Library

This is a small library with media materials on programming topics. 
In order to view materials in the library, just click on the preferred item -
thus you will be redirected to the source page.

#### Solution Stack:

- SASS
- Gulp (gulp-babel, gulp-autoprefixer)
- Font-awesome

#### Basic functionality provided on the web page:

- Modal windows on Add Item, Sign Up and Sign In buttons.
- Filters.
- Elements search by Title.
- Ability to add materials to the library.
- Routing through the pages (contacts, gallery...).
- Save liked materials to the Favourite.
- Header slider.

#### Type of filters available:

- Category (video/article/course)
- Language (C++, JS, Python, etc...)
- Level (Beginner, Intermediate, Advanced)
- Title Search

#### How to add elements to the library:

- Click on Add Item button;
- Fill the fields correspondingly;
- Provide link to the outer source depending on the category (video / other...)
    1) YouTube link (for video e.g. `https://www.youtube.com/watch?v=6NPfQJJEySY`)
    2) Any other link (for articles / courses);
- Click submit button;
- Element will be added to the library asap;

##### P.S. You don't have to provide images for your materials, they will be added automatically depending on the language choise.

#### P.P.S. Local Storage is used in order to store the data. 