const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');

const browserSync = require('browser-sync').create();


gulp.task('html', function () {
    return gulp.src('./src/index.html')
        .pipe(gulp.dest('./build/'))
});

gulp.task('pages', function () {
    return gulp.src('./src/views/*.html')
        .pipe(gulp.dest('./build/views'))
});


gulp.task('scss', function() {
    return gulp.src('./src/assets/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
        .pipe(gulp.dest('./build/assets/css'));
});


gulp.task('js', function() {
    return gulp.src('./src/js/**/*.js')
        .pipe(babel({
            "presets": [
                [
                    "@babel/preset-env",
                    {
                        "targets": {
                            ie: "11",

                        },
                        modules: false
                    }
                ]
            ]
        }
        ))
        .pipe(gulp.dest('./build/js/'));
});

gulp.task('img', function() {
    return gulp.src('./src/assets/img/**/*.png')
        .pipe(gulp.dest('./build/assets/img/'));
});

gulp.task('normalize', function() {
    return gulp.src('./src/assets/normalize/normalize.css')
        .pipe(gulp.dest('./build/assets/normalize'));
});

gulp.task('libs', function() {
    return gulp.src('./src/libs/**/*')
        .pipe(gulp.dest('./build/libs/'));
});


gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
    browserSync.watch('./src/', browserSync.reload)
});



gulp.task('cleanBuild', function () {
    return gulp.src('./build', {allowEmpty: true})
        .pipe(clean())
});


gulp.task('watch', function() {
    gulp.watch('./src/index.html', gulp.series('html'));
    gulp.watch('./src/views/**/*.html', gulp.series('pages'));
    gulp.watch('./src/js/**/*.js', gulp.series('js'));
    gulp.watch('./src/assets/scss/**/*.scss', gulp.series('scss'));
    gulp.watch('./src/assets/img/**/*.png', gulp.series('img'));
    gulp.watch('./src/assets/normalize/normalize.css', gulp.series('normalize'));
});

gulp.task('default', gulp.series(
    'cleanBuild',
    gulp.parallel('normalize', 'pages', 'libs', 'html', 'scss', 'js', 'img'),
    gulp.parallel('watch', 'serve')

));
